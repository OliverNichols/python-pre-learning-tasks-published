def vowel_swapper(string):
    # ==============
    if not isinstance(string, str): raise ValueError("string parameter must be str")
    import re # Import module for regex - easier to use than str.replace in this context in my opinion
    
    vowel_map = {'a':'4', 'e':'3', 'i':'!', 'o':'ooo', 'u':'|_|'} # Our defining map
    return re.subn('[aeiouAEIU]', lambda x:vowel_map.get(x.group()[0].lower()), string)[0].replace('O','000') # Using re.subn (essentially a regex replace function) we pass the map defined above for vowels but exclude the capital O (replaced after the fact)

    # ==============

print(vowel_swapper("aA eE iI oO uU")) # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World")) # Should print "H3llooo Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "3v3ryth!ng's 4v4!l4bl3" to the console


# Redefining for stretched function to make the original solution more readable
def second_vowel_swapper(string):
    # ==============
    if not isinstance(string, str): raise ValueError("string parameter must be str")
    import re, collections as cl
    
    vowel_map = {'a':'4', 'e':'3', 'i':'!', 'o':'ooo', 'u':'|_|'} # Our defining map
    vowel_count = cl.Counter() # Counter class is useful for counting vowels as we use them
    return re.subn('[aeiouAEIOU]', lambda x:vowel_map.get((original:=vowel.replace('O','000')).lower(), original) if vowel_count.__iadd__({(vowel:=x.group()[0]).lower():1})[vowel.lower()]==2 else vowel, string)[0] # Same as before but this time it first adds to the counter using __iadd__ method - if the count isn't exactly 2 for this vowel (after addition), it won't replace. Re-arranged placement of capital O replacement such that it is only replaced on second count

    # ==============

print(second_vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a4a e3e i!i o000o u|_|u" to the console
print(second_vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(second_vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av4!lable" to the console

