def factors(number):
    # ==============
    if not isinstance(number, int) or number <= 1: return f"{number} cannot be a prime number"
    import math
    
    return sorted([n for x in range(math.ceil(number**0.5), 1+number//2) for n in set((x,number//x)) if not number%x]) or f"{number} is a prime number" # Check all x between its sqrt (rounded up to avoid repetitions) and itself divided by 2 - condition to check if its mod with x is 0, then also adds the division result as this must by definition also be a result
    # ==============

# Includes stretched task

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “13 is a prime number” to the console

print(factors(9)) # Should print [3] to the console - checking for avoiding  at sqrt
print(factors(1)) # Should print “1 cannot be a prime number” to the console - extended
