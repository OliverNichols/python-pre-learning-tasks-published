def calculator(a, b, operator):
    # ==============
    if any(not isinstance(n, int) for n in (a, b)): raise TypeError("parameters a and b must be int")
    if not isinstance(operator, str): raise TypeError("operator parameter must be str")
    
    op = __import__('operator') # Avoid overwriting parameter operator and reduce confusion
    op_map = {'+':op.add, '-':op.sub, '*':op.mul, '/':op.floordiv} # floordiv immediately sets div result to int

    if operator in op_map: return op_map.get(operator)(a, b)
    else: raise ValueError("invalid operator") # Raise if operator given is not supported, ie is not + - * /

    # ==============

print(calculator(2, 4, "+")) # Should print 6 to the console
print(calculator(10, 3, "-")) # Should print 7 to the console
print(calculator(4, 7, "*")) # Should print 28 to the console
print(calculator(100, 2, "/")) # Should print 50 to the console


# Stretch for binary result
binary = lambda number: int(bin(number).lstrip('-0b')) # bin returns string leading with -?0b, so we strip this and convert to int (loses sign for negative)

print(binary(calculator(2, 4, "+"))) # Should print 110 to the console
print(binary(calculator(10, 3, "-"))) # Should print 111 to the console
print(binary(calculator(4, 7, "*"))) # Should print 11100 to the console
print(binary(calculator(100, 2, "/"))) # Should print 110010 to the console

print(binary(calculator(15, 2, "/"))) # Should print 111 to the console


# Alternatively, non built-in solution with
def binary(number):
    if not isinstance(number, int) or number < 0: raise ValueError("number parameter must be a non-negative int")

    bits = []
    while number:
        bits.append(str(number%2))
        number //= 2

    return int(''.join(reversed(bits)))
